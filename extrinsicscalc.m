colorVid = videoinput('kinect', 1, 'RGB_640x480');
triggerconfig(colorVid,'manual');

colorVid.FramesPerTrigger = 1;
start(colorVid);

imOrig = getsnapshot(colorVid);
imOrig = undistortImage(imOrig, cameraParams);
[imagePoints, boardSize] = detectCheckerboardPoints(imOrig);
squareSize = 28; % in millimeters
worldPoints = generateCheckerboardPoints(boardSize, squareSize);
%cameraParams = estimateCameraParameters(imagePoints, worldPoints);
im = imOrig;
im = undistortImage(imOrig, cameraParams);

[imagePoints, boardSize] = detectCheckerboardPoints(im);
[rotationMatrix, translationVector] = extrinsics(imagePoints, worldPoints, cameraParams);
figure;
plot3(worldPoints(:,1), worldPoints(:,2),zeros(size(worldPoints, 1),1),'*');
hold on
plot3(0,0,0,'g*');
orientation = rotationMatrix';
location = -translationVector * orientation;
cam = plotCamera('Location',location,'Orientation',orientation,'Size',20);
set(gca,'CameraUpVector',[0 0 -1]);
camorbit(gca,-110,60,'data',[0 0 1]);
axis equal
grid on
cameratoolbar('SetMode','orbit');
dist = sqrt(location(1)^2+location(2)^2+location(3)^2)
r = vrrotmat2vec(orientation);
r(4)*180/pi
stop(colorVid); flushdata(colorVid)