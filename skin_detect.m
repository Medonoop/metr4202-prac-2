%Detection of Face on the basis of skin color
colorVid = videoinput('kinect', 1, 'RGB_640x480');
triggerconfig(colorVid,'manual');

colorVid.FramesPerTrigger = 1;
start(colorVid);

cform = makecform('srgb2lab');
clc;

fudgeFactor = 0.5;
se90 = strel('line', 3, 90);
se0 = strel('line', 3, 0);
figure; hold on
while ~islogging(colorVid)
    
    frame=getsnapshot(colorVid);
    
    %subplot(2,2,1)
    %imshow(frame)
    
    J = applycform(frame,cform);
    %subplot(2,2,2)
    %imshow(J);
    
    K=J(:,:,2);
    L=graythresh(J(:,:,2));
    BW1=im2bw(J(:,:,2),L);
    M=graythresh(J(:,:,3));
    BW2=im2bw(J(:,:,3),M);
    %subplot(2,2,2)
    %imshow(BW2);
    O=BW1.*BW2;
    
    % Bounding box
    P=bwlabel(O,8);  % 8||4
    BB=regionprops(P,'Boundingbox');
    BB1=struct2cell(BB);
    BB2=cell2mat(BB1);

    [s1, s2]=size(BB2);
    maxArea=0;
    j = 3;
    for k=3:4:s2-1
        p=BB2(1,k)*BB2(1,k+1);
        if p>maxArea && p > 20000
            maxArea_old = maxArea;
            maxArea=p;
            j_old = j;
            j=k;
        end
    end
    
    imshow(frame);
    box=[BB2(1,j-2),BB2(1,j-1),BB2(1,j),BB2(1,j+1)]; % [x,y,w,h]
    %box2=[BB2(1,j_old-2),BB2(1,j_old-1),BB2(1,j_old),BB2(1,j_old+1)]; % [x,y,w,h]
    rectangle('Position',box,'EdgeColor','r' )
    %rectangle('Position',box2,'EdgeColor','g' )
        
    pause(0.005)
    
end

