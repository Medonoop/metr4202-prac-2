%%% calibrate

colorVid = videoinput('kinect', 1, 'RGB_640x480');
depthVid = videoinput('kinect',2);

% Set the triggering mode to 'manual'
triggerconfig([colorVid depthVid],'manual');
colorVid.FramesPerTrigger = 1;
depthVid.FramesPerTrigger = 1;

start([colorVid depthVid]);

pause(1);  % helps to sync depth and color camera

disp('capturing')

figure;
hold on

for n=1:20
    
    disp('capturing')
    frame = getsnapshot(colorVid);
    imshow(frame)
    filename = sprintf('cal%d.jpg',n);
    imwrite(frame,filename);

    pause(0.7)
    
end

close all

% Stop the devices
stoppreview(colorVid)
stoppreview(depthVid)

stop([colorVid depthVid]);

delete([colorVid depthVid]);

disp('calibrating')

%%%%%%update matlab current folder 
cameraCalibrator('C:\Users\Andrew\Documents\MATLAB',26)

%cameracalibratorapp



